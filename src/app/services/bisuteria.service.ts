import { Injectable, inject } from '@angular/core';
import { Firestore, addDoc, collection, collectionData, doc, getDocs, query, updateDoc, where, deleteDoc, setDoc } from '@angular/fire/firestore';
import { Bisuteria } from '../commons/interfaces';
import { Observable } from 'rxjs';
import { DataList } from '../customers/data';

@Injectable({
  providedIn: 'root'
})
export class BisuteriaService {

  private firestore = inject(Firestore)

  data: Bisuteria[] = DataList;
  collection: string = 'bisuteria';

  getBisuteria(): Observable<Bisuteria[]> {
    const data = collection(this.firestore, this.collection)

    return collectionData(data) as unknown as Observable<Bisuteria[]>
  }

  async addBisuteria(data: Bisuteria) {
    const collectionRef = collection(this.firestore, this.collection);
    const newDocRef = await addDoc(collectionRef, data);
    const idDb = newDocRef.id;
    const docRef = doc(this.firestore, this.collection, idDb);
    
    await setDoc(docRef, { ...data, idDb });
  }

  deleteBisuteria(data: Bisuteria) {
    console.log({delete: data})

    const documentRef = doc(this.firestore, this.collection, data.idDb);

    return deleteDoc(documentRef)
  }

  async updateBisuteria(data: Bisuteria) {
    try {
      const collectionRef = collection(this.firestore, this.collection);
      const q = query(collectionRef, where('idDb', '==', data.idDb));
      const querySnapshot = await getDocs(q);

      if (querySnapshot.size === 0) {
        throw new Error(`No se encontró la bisutería con id ${data.idDb}`);
      }

      const document = querySnapshot.docs[0];
      const docRef = doc(this.firestore, this.collection, document.id);

      await updateDoc(docRef, { ...data });
    } catch (error) {
      console.error('Error al actualizar bisutería:', error);
    }
  }

  async insertBulkData() {
    this.data.forEach(async (data) => {
      await this.addBisuteria({ ...data })
    })
  }

  async deleteAllBisuteria() {
    const collectionRef = collection(this.firestore, this.collection);
    const querySnapshot = await getDocs(collectionRef);

    querySnapshot.forEach((doc) => {
      deleteDoc(doc.ref);
    });
  }

}
