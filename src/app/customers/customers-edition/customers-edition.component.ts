import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Bisuteria } from 'src/app/commons/interfaces';

@Component({
  selector: 'app-customers-edition',
  templateUrl: './customers-edition.component.html',
  styleUrls: ['./customers-edition.component.scss']
})
export class CustomersEditionComponent {

  @Input() item!: Bisuteria;

  @Output() onDelete: EventEmitter<Bisuteria> = new EventEmitter<Bisuteria>(false);
  @Output() onSoldOut: EventEmitter<Bisuteria> = new EventEmitter<Bisuteria>(false);
  @Output() onInStock: EventEmitter<Bisuteria> = new EventEmitter<Bisuteria>(false);
  @Output() onUpdateArticle: EventEmitter<Bisuteria> = new EventEmitter<Bisuteria>(false);

}
