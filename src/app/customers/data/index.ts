import { Bisuteria } from "src/app/commons/interfaces";

export const DataList: Bisuteria[] = [
   {
      title: "Pulsera de plata de ley 925 con forma de estrella para mujer, brazalete de cuentas redondas con personalidad, moda coreana",
      image: "https://ae01.alicdn.com/kf/S5df4414e35d34ddbbedf8f45a45d4e6cX.jpg_450x450.jpg",
      price: 1,
      check: false,
   }
]