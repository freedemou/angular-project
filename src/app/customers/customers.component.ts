import { Component, ElementRef, OnDestroy, OnInit, Renderer2, inject } from "@angular/core";
import { Bisuteria, BreakpointsMediaQuery } from "../commons/interfaces";
import { BisuteriaService } from "../services/bisuteria.service";
import { Observable, Subject, map, takeUntil } from "rxjs";
import { Router } from "@angular/router";
import { SnackbarService } from "../commons/services";
import { BreakpointObserver, BreakpointState, Breakpoints } from "@angular/cdk/layout";
import { ModalService } from "../commons/services/modal.service";
import { CustomersAddComponent } from "./customers-add/customers-add.component";

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  private bisuteriaService = inject(BisuteriaService)
  private route = inject(Router)
  private snackbarService = inject(SnackbarService);
  private breakpointObserver = inject(BreakpointObserver)
  private modalService = inject(ModalService)

  searchHash: string = '';
  bisuteria$!: Observable<Bisuteria[]>;
  isLoading: boolean = true;
  bisuteria: Bisuteria[] = [];
  checked: boolean = false;
  key: string = 'GdOqqhXgAhA30dUlpWwM';
  listIdsChecked: number[] = [];
  currentScreenSize!: string;
  displayNameMap = new Map([
    [Breakpoints.XSmall, BreakpointsMediaQuery.XSmall],
    [Breakpoints.Small, BreakpointsMediaQuery.Small],
    [Breakpoints.Medium, BreakpointsMediaQuery.Medium],
    [Breakpoints.Large, BreakpointsMediaQuery.Large],
    [Breakpoints.XLarge, BreakpointsMediaQuery.XLarge],
  ]);

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
    this.getBisuteria();
    this.initBreakpointObserver();

    setTimeout(() => {
      //this.bisuteriaService.insertBulkData();
    }, 5000)
  }

  searchByAttribute(): void {
    console.log(this.searchHash);

    const items: HTMLElement[] = this.el.nativeElement.querySelectorAll('.layout__row');

    items.forEach((item: HTMLElement) => {
      const itemHash = item.getAttribute('hash');
      this.renderer.removeClass(item, 'layout__row--hide');

      if (itemHash !== this.searchHash) {
        this.renderer.addClass(item, 'layout__row--hide');
      }
    });

    if (this.searchHash === '') {
      items.forEach((item: HTMLElement) => this.renderer.removeClass(item, 'layout__row--hide'));
    }
  }

  initBreakpointObserver(): void {
    this.breakpointObserver
      .observe([
        Breakpoints.XSmall,
        Breakpoints.Small,
        Breakpoints.Medium,
        Breakpoints.Large,
        Breakpoints.XLarge
      ])
      .subscribe((state: BreakpointState) => {
        for (const query of Object.keys(state.breakpoints)) {
          if (state.breakpoints[query]) {
            this.currentScreenSize = this.displayNameMap.get(query) ?? BreakpointsMediaQuery.Unknown;
          }
        }
      })
  }

  get isBreakpointsSmall(): boolean {
    return this.isEdit && [
      BreakpointsMediaQuery.XSmall.toString(),
      BreakpointsMediaQuery.Small.toString()
    ].includes(this.currentScreenSize)
  }

  get isEdit(): boolean {
    return this.route.url?.split('?')[1]?.split('=')[1] === this.key;
  }

  private getBisuteria(): void {
    this.bisuteriaService.getBisuteria()
      .pipe(
        takeUntil(this.unsubscribe$),
        map( data => this.sortDataAsc(data, 'ASC'))
      )
      .subscribe(data => {
        console.log(data);

        this.bisuteria = data;
        this.isLoading = false;
      })
  }

  sortDataAsc(data: Bisuteria[], order: 'ASC' | 'DESC'): Bisuteria[] {
    return data.sort((a, b) => {
      return order === 'ASC' ? Number(a.price) - Number(b.price) : Number(b.price) - Number(a.price)
    })
  }

  get totalCheckedItems(): number {
    return this.bisuteria.filter(({ check }) => check).length
  }

  onChecked(id: number, data: Bisuteria): void {
    if( data.check ) {
      this.listIdsChecked.push(id)
    } else {
      const indexId = this.listIdsChecked.findIndex(code => code === id)
      this.listIdsChecked.splice(indexId, 1)
    }
  }

  get listIdsCheckedItems(): string {
    return this.listIdsChecked.join(', ');
  }

  async delete(data: Bisuteria): Promise<void> {
    if( confirm('Estas seguro de eliminar el producto seleccionado?') ) {
      try {
        await this.bisuteriaService.deleteBisuteria(data)

        this.snackbarService.showMessage({
          message: 'Producto eliminado correctamente',
          action: 'Cerrar',
          duration: 5
        })
      } catch (err) {
        console.log(err)
      }
    }
  }

  get textIsManyItemSelected(): string {
    return this.totalCheckedItems > 1 ? 's' : '';
  }

  get textSoldOut(): string {
    return 'Agotado';
  }

  async soldOut(data: Bisuteria): Promise<void> {
    await this.updateProcess({ ...data, sold: true });
    
    this.snackbarService.showMessage({
      message: 'Producto agotado',
      action: 'Cerrar',
      duration: 5
    })
  }

  async inStock(data: Bisuteria): Promise<void> {
    await this.updateProcess({ ...data, sold: false });

    this.snackbarService.showMessage({
      message: 'Producto En Stock',
      action: 'Cerrar',
      duration: 5
    })
  }

  async updateArticle(data: Bisuteria): Promise<void> {
    await this.updateProcess({ ...data })

    this.snackbarService.showMessage({
      message: 'Producto actualizado',
      action: 'Cerrar',
      duration: 5
    })
  }

  async updateProcess(data: Bisuteria): Promise<void> {
    await this.bisuteriaService.updateBisuteria(data);
  }

  itemTrackBy(index: number, item: Bisuteria): number {
    return item.id || index;
  }

  addProduct(): void {
    this.modalService.openModal({
      content: CustomersAddComponent,
      params: { id: 1200 }
    })
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
