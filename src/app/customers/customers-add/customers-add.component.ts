import { Component, inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Bisuteria } from 'src/app/commons/interfaces';
import { SnackbarService } from 'src/app/commons/services';
import { BisuteriaService } from 'src/app/services';
@Component({
  selector: 'app-customers-add',
  templateUrl: './customers-add.component.html',
  styleUrls: ['./customers-add.component.scss']
})
export class CustomersAddComponent {

  private fb = inject(FormBuilder)
  private bisuteriaService = inject(BisuteriaService)
  private snackbarService = inject(SnackbarService);

  public bsModalRef = inject(BsModalRef)
  public isLoading: boolean = false;

  public form = this.fb.group({
    image: ["", Validators.required],
    price: [0, Validators.required],
    title: ["", Validators.required],
    check: [false],
    sold: [false],
  });

  async onSubmit(): Promise<void> {
    if( this.form.invalid) {
      return;
    }

    try {
      this.isLoading = true;
      await this.bisuteriaService.addBisuteria(this.form.value as Bisuteria)
      this.form.reset();
      this.bsModalRef.hide();
      this.snackbarService.showMessage({
        message: 'Producto agregado correctamente',
        action: 'Cerrar',
        duration: 5
      })
    } catch (err) {
      console.log(err);
      this.snackbarService.showMessage({
        message: 'Ocurrio un error en el servidor, inténtalo nuevamente.',
        action: 'Cerrar',
        duration: 5
      })
    } finally {
      this.isLoading = false;
    }
  }

}
