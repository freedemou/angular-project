interface MenuItem {
  name: string;
  path: string;
}

import { Component, signal } from '@angular/core';

@Component({
  selector: 'app-signal',
  templateUrl: './signal.component.html',
  styleUrl: './signal.component.scss'
})
export class SignalComponent {

  public menuList = signal<MenuItem[]>([
    { name: 'Home', path: '/signal/home' },
    { name: 'About', path: '/signal/about' },
    { name: 'Contact', path: '/signal/contact' },
  ])

}
