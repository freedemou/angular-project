import { Injectable, inject } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

export interface ModalContent {
  content: any;
  params?: any;
}

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private modalService = inject(BsModalService)
  private modalRef!: BsModalRef;

  openModal(modal: ModalContent) {
    const { content, params } = modal;

    if (typeof content === 'object') {
      this.modalRef = this.modalService.show(content, {
        initialState: params,
        backdrop: 'static',
        keyboard: false
      })
    } else {
      this.modalRef = this.modalService.show(content);
    }
  }

  closeModal() {
    this.modalRef.hide();
  }

}
