import { Injectable, inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarMessage } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  private snackBar = inject(MatSnackBar);

  showMessage(params: SnackbarMessage): void {
    this.snackBar.open(params.message, params.action, {
      duration: params.duration * 1000
    });
  }
}
