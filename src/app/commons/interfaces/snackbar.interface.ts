export interface SnackbarMessage {
    message: string;
    action?: string;
    duration: number;
}