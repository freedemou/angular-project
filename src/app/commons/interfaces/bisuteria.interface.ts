export interface Bisuteria {
    image: string;
    price: number | string;
    title: string;
    check: boolean;
    id?: number;
    sold?: boolean;
    idDb?: any;
}

export enum BreakpointsMediaQuery {
    XSmall = 'XSmall',
    Small = 'Small',
    Medium = 'Medium',
    Large = 'Large',
    XLarge = 'XLarge',
    Unknown = 'Unknown'
}