# Usa una imagen base de Node.js para construir la aplicación Angular
FROM node:16 as build

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia el archivo de configuración del proyecto (package.json y package-lock.json)
COPY package*.json ./

# Instala las dependencias
RUN npm install

# Copia el resto de los archivos del proyecto
COPY . .

# Compila la aplicación Angular
RUN npm run build

# Configura una imagen de Nginx para servir la aplicación compilada
FROM nginx:alpine

# Copia los archivos compilados de Angular al directorio de distribución de Nginx
COPY --from=build /app/dist/angular-test /usr/share/nginx/html
